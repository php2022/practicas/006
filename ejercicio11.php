<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <style>
            table,td{
                border: 1px solid black;
                border-collapse: collapse;
            }
            
            td{
                padding: 20px
            }
            
            td:first-child{
                text-align: right;
                background-color: rgb(135,235,149);
                width: 200px;
            }
            
            td:nth-child(2){
                background-color: lightgrey;
                width: 300px;
            }
            
            input,textarea,select,option{
                width: 85%;
            }
            
            textarea{
                height: 100px
            }
            .cumple{
                width: 20%;
            }
            
            #h{
                width: 4%
            }
            
            #FI,#AC,#SU,#TE,#CO{
                width: 4%
            }
            
            #boton{
                width: 15%;
                margin: 10px 43%;
            }
            
            body{
                background-color: bisque;
            }
            
            table{
                margin: 0 auto;
            }
            
            h1,#boton{
                text-align: center;
            }
            
        </style>
    </head>
    <body>
        <h1>Formulario de inscripción de usuarios</h1>
        <form>
            <table>
                <tbody>
                    <tr>
                        <td>Nombre Completo</td>
                        <td>
                            <input type="text" id="numero" name="numero" value=""/>
                        </td>
                    </tr>
                    <tr>
                        <td>Dirección</td>
                        <td>
                            <textarea></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td>Correo electrónico</td>
                        <td>
                            <input type="email" id="email" name="email">
                        </td>
                    </tr>
                    <tr>
                        <td>Contraseña</td>
                        <td>
                            <input type="password" id="contraseña" name="contraseña">
                        </td>
                    </tr>
                    <tr>
                        <td>Confirmar Contraseña</td>
                        <td>
                            <input type="password" id="contraseña" name="contraseña">
                        </td>
                    </tr>
                    <tr>
                        <td>Fecha de nacimiento</td>
                        <td>
                            <input type="date" id="date" name="date">
                            
                        </td>
                    </tr>
                    <tr>
                        <td>Sexo</td>
                        <td>
                            <input type="radio" name="h" id="h" value=""/>Hombre<br>
                            <input type="radio" name="h" id="h" value=""/>Mujer
                        </td>
                    </tr>
                    <tr>
                        <td>Por favor elige los temas de tus intereses</td>
                        <td>
                            <input type="checkbox" name="intereses[]" id="FI" value="FI"/>
                            <label for="FI">Ficción</label><br>
                            <input type="checkbox" name="intereses[]" id="AC" value="AC"/>
                            <label for="AC">Acción</label><br>
                            <input type="checkbox" name="intereses[]" id="SU" value="SU"/>
                            <label for="SU">Suspense</label><br>
                            <input type="checkbox" name="intereses[]" id="TE" value="TE"/>
                            <label for="TE">Terror</label><br>
                            <input type="checkbox" name="intereses[]" id="CO" value="CO"/>
                            <label for="CO">Comedia</label>
                        </td>
                    </tr>
                    <tr>
                        <td>Selecciona tus aficiones<br><br>(Selecciona múltiples elementos pulsando la tecla Control y haciendo clic en cada uno, uno a uno)</td>
                        <td>
                            <select multiple name="aficiones[]">
                                <option value="AL">Deportes al aire libre</option>
                                <option value="AV">Deportes de aventuras</option>
                                <option value="PO">Música Pop</option>
                                <option value="RO">Música Rock</option>
                                <option value="ALT">Música Alternativa</option>
                                <option value="FO">Fotografía</option>
                            </select>
                        </td>
                    </tr>
                </tbody>
            </table>
            <input type="submit" value="Enviar" name="boton" id="boton"/>
        </form>
        
        
        <?php
        
        ?>
    </body>
</html>
